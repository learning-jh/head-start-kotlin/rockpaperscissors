# Chapter 3 exercises

## Be the compiler

### A

Seems fine.

### B

Won't compile, as assigning new value to `x`.

### C

Won't compile, as not specifying return type.
